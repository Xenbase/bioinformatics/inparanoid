# InParanoid

# Materials
Source: https://bitbucket.org/sonnhammergroup/inparanoid/src/master/ 
Paper (original InParanoid) https://doi.org/10.1093/nar/gki107 
Paper (InParanoid-DIAMOND) https://doi.org/10.1093/bioinformatics/btac194
DIAMOND is an ultrafast BLAST alternative.
https://fgr.hms.harvard.edu/diopt (DIOPT)


# Download 2023_06 proteomes for XENLA and XENTR (ID: xenopus, PW: ribbit)
https://pub.amphibase.org/annotation/2023_06/XENTR_XB202306.prot_all.fa.gz 
https://pub.amphibase.org/annotation/2023_06/XENLA_XB202306.prot_all.fa.gz 

# Download human proteomes
https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_45/gencode.v45.pc_translations.fa.gz

# Install Inparanoid: 
git clone https://bitbucket.org/sonnhammergroup/inparanoid.git 


# Take the longest protein sequences per gene

Use this python script: https://gitlab.com/Xenbase/bioinformatics/inparanoid/-/blob/main/Find_longest_proteins.ipynb?ref_type=heads

# Split L/S proteins from XENLA proteins

Use this python script: https://gitlab.com/Xenbase/bioinformatics/inparanoid/-/blob/main/Split_L_and_S.ipynb?ref_type=heads

# Run InParanoid for XENLA.L-XENLA.S, XENLA.L-XENTR, XENLA.S-XENTR, XENTR-Human
perl inparanoid.pl XENLA.L.fasta XENLA.S.fasta -out-table

# Data wrangling after running InParannoid
Useful Python scripts: https://gitlab.com/Xenbase/bioinformatics/inparanoid/-/tree/main/Python%20scripts?ref_type=heads

# Final tables after data wrangling
https://drive.google.com/drive/folders/1MzEFBHORinAnxSDg0yWveCL45uUAfdVi?usp=drive_link




